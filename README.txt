Ramin Anushiravani ramin.audio@gmail.com 

This is a speech to text python code written in Jupiter Notebook. Make sure you're using python3 and have jupyter notebook installed. 

Open "Transcribing Audio.ipynb". Many libraries need to be installed, most of them can be simply installed with pip. More explanation on what all the other files are is provided in the python notebook. 

Have fun!